# -*- coding: utf-8 -*-
'''
Created on 13/05/2014
@author: david, hector, marrao, mundi
'''

from bottle import route

# framework
from bottle import default_app

# controllers
from controllers import resources
from controllers import priceprofor_graficas
from controllers import priceprofor_single_plots
from controllers import sme_mde_viz
from controllers import priceprofor_RESTful_API
from controllers import priceprofor_precioshorarios

from bottle import TEMPLATE_PATH, static_file
# TEMPLATE_PATH.append(os.path.join(os.environ['OPENSHIFT_HOMEDIR'],'runtime/repo/wsgi/views/'))

# Get APP root in system
from os import path as ospath
from os import environ as osenviron

# get sys to add proper template paths
import sys

try:
    root_app=ospath.join(osenviron['OPENSHIFT_REPO_DIR'])
except:
    root_app=ospath.join(sys.path[0])
finally:
    TEMPLATE_PATH.append(ospath.join(root_app, 'wsgi', 'views', 'templates'))
    TEMPLATE_PATH.append(ospath.join(root_app, 'wsgi', 'views', 'templates','priceprofor_graficas'))
    TEMPLATE_PATH.append(ospath.join(root_app, 'wsgi', 'views', 'templates','priceprofor_RESTful_API'))

CONN_URI=None

#from flask import Flask, url_for, redirect

@route("/")
# @app.route("/index")
# @app.route("/index/")
# @app.route("/index.html")
# @app.route("/home")
# @app.route("/home/")
# @app.route("/home.html")
def roothomeindex():
    """
    Default Project presentation page.
    """
    # print url_for('static', filename="index.html")
    return static_file('index.html', root=ospath.join(root_app,'wsgi','static', 'homefolder'))

@route('/homefolder/<path:path>')
def callback(path):
    return static_file(path, root=ospath.join(root_app,'wsgi','static', 'homefolder'))



myapplication = default_app()

